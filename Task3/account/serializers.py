from rest_framework import serializers
from . models import Course, Subject
from django.contrib.auth import get_user_model
User = get_user_model()

#Subject serialiser
class SubjectSerializer(serializers.ModelSerializer):
  class Meta:
    model = Subject
    fields = ['subject_code','subject_name']
    
#Course serialiser
class CourseSerializer(serializers.ModelSerializer):
  class Meta:
    model = Course
    fields = ['id','course_name','subject']
    depth = 1

  def update(self, instance, validated_data):
    print(validated_data)
    instance.name = validated_data.get('name', instance.name)
    instance.save()
    return instance
         
    
#user serialiser
class UserSignUpSerializer(serializers.ModelSerializer):
  courses = CourseSerializer(many=True, read_only = False) 
  class Meta:
    model = User
    fields = ['email','password','name', 'phone', 'address', 'gender', 'department', 'date_of_birth', 'role', 'courses',]  
  
  def create(self, validated_data):
    courses = validated_data.pop('courses')
    user = User.objects.create(email=validated_data['email'])
    user.set_password(validated_data['password'])
    user.name = validated_data['name']
    user.phone = validated_data['phone']
    user.address = validated_data['address']
    user.gender = validated_data['gender']
    user.department = validated_data['department']
    user.date_of_birth = validated_data['date_of_birth']
    user.groups.add(2)
    user.role = validated_data['role']
    user.save()
    if validated_data['role'] == 'student':
      for course in courses:
        c_obj = Course.objects.get(course_name=course['course_name'])
        user.courses.add(c_obj)
    return user 

  def update(self, instance, validated_data):
    courses = validated_data.pop('courses')
    instance.email = validated_data.get('email', instance.email)
    instance.set_password(validated_data.get('password', instance.password))
    instance.name = validated_data.get('name', instance.name)
    instance.phone = validated_data.get('phone', instance.phone)
    instance.address = validated_data.get('address', instance.address)
    instance.gender = validated_data.get('gender', instance.gender)
    instance.department = validated_data.get('department', instance.department)
    instance.date_of_birth = validated_data.get('date_of_birth', instance.date_of_birth)
    instance.role = validated_data.get('role', instance.role)
    instance.save()
    for course in courses:
      c_obj = Course.objects.get(course_name=course['course_name'])
      instance.courses.add(c_obj)
    return instance    

class TeacherSignUpSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = ['email','password','name', 'phone', 'address', 'gender', 'department', 'date_of_birth', 'role']  

  def create(self, validated_data):
    user = User.objects.create(email=validated_data['email'])
    user.set_password(validated_data['password'])
    user.name = validated_data['name']
    user.phone = validated_data['phone']
    user.address = validated_data['address']
    user.gender = validated_data['gender']
    user.department = validated_data['department']
    user.date_of_birth = validated_data['date_of_birth']
    user.groups.add(1)
    user.role = validated_data['role']
    user.save()
    return user 

  def update(self, instance, validated_data):
    instance.email = validated_data.get('email', instance.email)
    instance.set_password(validated_data.get('password', instance.password))
    instance.name = validated_data.get('name', instance.name)
    instance.phone = validated_data.get('phone', instance.phone)
    instance.address = validated_data.get('address', instance.address)
    instance.gender = validated_data.get('gender', instance.gender)
    instance.department = validated_data.get('department', instance.department)
    instance.date_of_birth = validated_data.get('date_of_birth', instance.date_of_birth)
    instance.role = validated_data.get('role', instance.role)
    instance.save()
    return instance    

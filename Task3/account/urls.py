from django.contrib import admin
from django.urls import path
from . import views
from rest_framework_simplejwt import views as jwt_views
urlpatterns = [
    path('u/',views.UserAPI.as_view(), name='u'),
    path('u/<int:id>/',views.UserAPI.as_view(), name='u'),
    path('course/',views.CourseAPI.as_view(), name='course'),
    path('course/<int:id>/',views.CourseAPI.as_view(), name='course'),
    path('subject/',views.SubjectAPI.as_view(), name='subject'),
    path('subject/<int:id>/',views.SubjectAPI.as_view(), name='subject'),
    path('gettoken/', jwt_views.TokenObtainPairView.as_view(), name='gettoken'),
    path('refresh/', jwt_views.TokenRefreshView.as_view(), name='refresh'),
]

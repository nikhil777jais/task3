from django.db import models
from django.contrib.auth.models import PermissionsMixin, AbstractBaseUser
from django.db.models.fields import DateField
from .managers import UserManager
from django.utils import timezone
import datetime

# Create your models here.
class User(AbstractBaseUser, PermissionsMixin):
  id = models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')
  email   = models.EmailField(max_length=50, unique=True,
                help_text='Email should be in prooper formate',)
  name    = models.CharField(max_length=30)
  phone   = models.IntegerField(null=True, blank=True)
  
  address = models.CharField(max_length=100, blank=True, null=True)
  GENDER_CHOICES = (
        ('male', 'male'),
        ('female', 'female'),
     )
  gender = models.CharField(max_length=10, choices=GENDER_CHOICES, default='NA')
  department   = models.CharField(max_length=50, blank=True, null=True)
  date_of_birth = DateField(default=datetime.date(2000, 1, 1))       
  PROFILE_CHOICES = (
        ('teacher', 'teacher'),
        ('student', 'student'),
        ('hod', 'hod'),
        ('admin', 'admin'),
    )
  role = models.CharField(max_length=20, choices=PROFILE_CHOICES)              
  date_joined = models.DateTimeField(default=timezone.now)
  is_active   = models.BooleanField(default=True,
                help_text='Designates whether this user should be treated as active. '
                          'Unselect this instead of deleting accounts.')
  is_staff    = models.BooleanField(default=False,
                help_text='Designates whether the user can log into this admin site.')

  objects = UserManager()
  
  USERNAME_FIELD = 'email'
  REQUIRED_FIELDS = ['name', 'phone', 'address', 'gender', 'date_of_birth']

  class Meta:
    verbose_name = 'user'
    verbose_name_plural = 'users'

class Subject(models.Model):
  subject_code = models.CharField(max_length=10, unique=True) 
  subject_name = models.CharField(max_length=50)

  def __str__(self):
      return self.subject_name
  

class Course(models.Model):
  user = models.ManyToManyField(User,  related_name='courses') 
  subject = models.ManyToManyField(Subject, related_name='subjects')  
  course_name = models.CharField(max_length=50) 

  def students(self):
    return " , ".join([ str(p) for p in self.user.all()])

  def subject_s(self):
    return " , ".join([ str(p) for p in self.subject.all()])

 
  
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Course, Subject
from django.contrib.auth import get_user_model

from account import serializers
User = get_user_model()
from .serializers import UserSignUpSerializer, TeacherSignUpSerializer, CourseSerializer, SubjectSerializer
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import DjangoModelPermissions
from rest_framework_simplejwt.tokens import RefreshToken


# Create your views here.
class SubjectAPI(APIView):
  authentication_classes = [JWTAuthentication]
  permission_classes = [DjangoModelPermissions]
  queryset = User.objects.none()
  def get(self, request, id = None, format=None): 
    if id != None:
      course_obj = Subject.objects.get(pk=id)
      serializer = SubjectSerializer(course_obj)
      return Response({'payload': serializer.data, 'msg':'Data Retrived'}, status = status.HTTP_200_OK)
    course_obj = Subject.objects.all()
    serializer = SubjectSerializer(course_obj, many=True)
    return Response({'payload': serializer.data, 'msg':'Data Retrived'}, status = status.HTTP_200_OK)
  
  def post(self, request, format = None):
    if request.user.role == 'student':
      return Response({'msg':'unauthorized access!!'}, status = status.HTTP_401_UNAUTHORIZED)
    serializer = SubjectSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
    return Response({'payload': serializer.data, 'msg':'Data Retrived'}, status = status.HTTP_200_OK)

class CourseAPI(APIView):
  authentication_classes = [JWTAuthentication]
  permission_classes = [DjangoModelPermissions]
  queryset = User.objects.none()
  
  def get(self, request, id = None, format=None):  
    if id != None:
      course_obj = Course.objects.get(pk=id)
      serializer = CourseSerializer(course_obj)
      return Response({'payload': serializer.data, 'msg':'Data Retrived'}, status = status.HTTP_200_OK)
    course_obj = Course.objects.all()
    serializer = CourseSerializer(course_obj, many=True)
    return Response({'payload': serializer.data, 'msg':'Data Retrived'}, status = status.HTTP_200_OK)

  def post(self, request, format = None):
    if request.user.role == 'student':
      return Response({'msg':'unauthorized access!!'}, status = status.HTTP_401_UNAUTHORIZED)
    course = Course.objects.create(course_name=request.data['course_name'])
    course.save()
    for sub in request.data['subject']:
      sub_obj = Subject.objects.get(subject_name=sub['subject_name'])
      course.subject.add(sub_obj)
    serializer = CourseSerializer(course)
    return Response({'payload': serializer.data, 'msg':'Data Retrived'}, status = status.HTTP_200_OK) 

#User API view
class UserAPI(APIView):
  authentication_classes = [JWTAuthentication]
  permission_classes = [DjangoModelPermissions]
  queryset = User.objects.none()

  def get(self, request, id = None, format=None):
    if request.user.role == 'student':
      c_name = Course.objects.get(user=request.user) #course of loged in user
      user_obj = User.objects.filter(courses=c_name) #user related to that course
      serializer = UserSignUpSerializer(user_obj, many=True)
      return Response({'payload': serializer.data, 'msg':'Data Retrived!!'}, status = status.HTTP_200_OK)
      
    if request.user.role == 'teacher':
      if id != None:
        user_obj = User.objects.filter(pk=id, role='student')
        if user_obj.exists():
          serializer = UserSignUpSerializer(user_obj.get(pk=id))
          return Response({'payload': serializer.data, 'msg':'Data Retrived!!'}, status = status.HTTP_200_OK)
        return Response({'msg':'Not found',}, status = status.HTTP_404_NOT_FOUND)  
      user_obj = User.objects.filter(role='student')
      serializer = UserSignUpSerializer(user_obj, many=True)
      return Response({'payload': serializer.data, 'msg':'Data Retrived!!'}, status = status.HTTP_200_OK)

    if request.user.role == 'admin':
      if id != None:
        user_obj = User.objects.get(pk=id)
        if user_obj.role == 'student':
          serializer = UserSignUpSerializer(user_obj)
        elif user_obj.role == 'teacher':
          serializer = TeacherSignUpSerializer(user_obj)
        return Response({'payload': serializer.data, 'msg':'Data Retrived!!'}, status = status.HTTP_200_OK)
      user_obj_t = User.objects.filter(role='teacher')
    user_obj_s = User.objects.filter(role='student')
    serializer_t =TeacherSignUpSerializer(user_obj_t, many= True)
    serializer_s =UserSignUpSerializer(user_obj_s, many= True)
    return Response({'payload':{'teacher':serializer_t.data, 'student':serializer_s.data},'msg':'Data Retrived!!'}, status = status.HTTP_200_OK)

  def post(self, request, format = None):
    if request.user.role != 'admin':
      return Response({'msg':'unauthorized access!!'}, status = status.HTTP_401_UNAUTHORIZED)
    if request.data['role'] == 'teacher':
      serializer = TeacherSignUpSerializer(data=request.data) 
    elif request.data['role'] == 'student':  
      serializer = UserSignUpSerializer(data=request.data) 
    if serializer.is_valid():
      serializer.save()
      user = User.objects.get(email= serializer.data['email']) #get newly generated user
      refresh = RefreshToken.for_user(user) #generate a token for user just while signup
      return Response({'payload': serializer.data , 'msg':'Data Saved!!','refresh': str(refresh), 'access': str(refresh.access_token),}, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)  

  def put(self, request, id = None, format = None):
    if request.user.role == 'student':
      return Response({'msg':'unauthorized access!!'}, status = status.HTTP_401_UNAUTHORIZED)
    elif request.user.role == 'teacher':
      user_obj = User.objects.filter(pk=id, role='student')
      if user_obj.exists():
        user = user_obj.get(pk=id)
        serializer = UserSignUpSerializer(user, data = request.data)
        if serializer.is_valid():
          serializer.save()
          return Response({'payload':serializer.data , 'msg':'Data Updated completely',}, status = status.HTTP_201_CREATED)
      return Response({'msg':'Not found',}, status = status.HTTP_404_NOT_FOUND)
    elif request.user.role == 'admin':
      user_obj = User.objects.get(pk=id)
      serializer = UserSignUpSerializer(user_obj, data = request.data) 
      if serializer.is_valid():
        serializer.save()
        return Response({'payload': serializer.data , 'msg':'Data Updated completely!!',}, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)  

  def put(self, request, id = None, format = None):
    if request.user.role == 'student':
      return Response({'msg':'unauthorized access!!'}, status = status.HTTP_401_UNAUTHORIZED)
    elif request.user.role == 'teacher':
      user_obj = User.objects.filter(pk=id, role='student')
      if user_obj.exists():
        user = user_obj.get(pk=id)
        serializer = UserSignUpSerializer(user, data = request.data, partial=True)
        if serializer.is_valid():
          serializer.save()
          return Response({'payload':serializer.data , 'msg':'Data Updated partially',}, status = status.HTTP_201_CREATED)
      return Response({'msg':'Not found',}, status = status.HTTP_404_NOT_FOUND)
    elif request.user.role == 'admin':
      user_obj = User.objects.get(pk=id)
      serializer = UserSignUpSerializer(user_obj, data = request.data, partial=True) 
      if serializer.is_valid():
        serializer.save()
        return Response({'payload': serializer.data , 'msg':'Data Updated partially',}, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)  

  def delete(self, request, id=None, format = None):
    if request.user.role != 'admin':
      return Response({'msg':'unauthorized access!!'}, status = status.HTTP_401_UNAUTHORIZED)
    user_obj = User.objects.get(pk=id)
    user_obj.delete()
    return Response({'msg':'Data Deleted Completely!!'}, status = status.HTTP_200_OK) 
      
